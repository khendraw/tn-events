<?php
 /*Template Name: New Template
 */

  get_header(); ?>
      <?php
      $mypost = array( 'post_type' => 'events', );
      ?>
        <div class="wrap">
          <?php while ( have_posts() ) : the_post(); ?>
              <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                  <header class="entry-header" style="border-bottom: 1px grey solid; margin-bottom:20px;">
                        <div style="display:inline-block;">
                          <h1><?php the_title(); ?></h1>
                          <span style = "margin-right:20px;"><?php $event_date = strtotime ( get_post_meta( get_the_ID(), 'event_date', true ) );?>
                            <span class="dashicons dashicons-calendar-alt"></span>
                            <?php echo date ("j F Y",$event_date);
                            ?>
                          </span>
                          <span>
                            <span class="dashicons dashicons-location"></span>
                            <?php echo get_the_term_list( $post->ID, 'event_location',' ', ', ', '. ' ) ?>
                          </span><br>
                          <?php echo get_the_term_list( $post->ID, 'event_target', 'Target Audience : ', ', ', '.  ' ) ?><br>
                          <?php echo get_the_term_list( $post->ID, 'event_industry', 'Industry : ', ', ', '.' ) ?>
                        </div>
                  </header>
                  <!-- Display event contents -->
                  <div class="entry-content">
                    <?php
                    /* if ( has_post_thumbnail() ) { ?>
                    <div style="float:left; margin-right:20px;">
                        <?php the_post_thumbnail(array(100,100));?>
                    </div>
                    <?php } */
                    the_content();
                    ?>
                  </div>
              </article>
          <?php endwhile; ?>
          <div style="display: block; border-style:solid; border-color:grey; border-width: 1px 0; padding: 20px;"
            <span style="float:left">
              <?php previous_post_link(); ?>
            </span>
            <span style="float:right">
              <?php next_post_link(); ?>
            </span>
          </div>
        </div>
      </div>
  <?php wp_reset_query(); ?>
  <?php get_footer(); ?>
