<?php get_header(); ?>
      <div class="wrap">
        <?php if ( have_posts() ) : ?>
            <header class="entry-header">
                <h1>Events</h1>
            </header>
              <div class="events-container">
                    <?php while ( have_posts() ) : the_post(); ?>
                    <div class="events-columns">
                    <!-- Display events -->
                        <a href="<?php the_permalink(); ?>">
                          <div class="img-placeholder">
                            <?php if ( has_post_thumbnail() ) {
                                  the_post_thumbnail();}
                                    else {?>
                                      <?php echo '<img src="' . plugins_url( 'no-image.png', __FILE__ ) .'">';
                            }?>
                          </div>
                        </a>
                        <div class="col-header">
                            <div style="float:left;">
                              <span class="dashicons dashicons-location" style="font-size:16px; color: #009933;"></span>
                              <?php echo get_the_term_list( $post->ID, 'event_location',' ', ', ', '  ' );?>
                            </div>
                            <?php
                            $event_date = strtotime ( get_post_meta( get_the_ID(), 'event_date', true ) );?>
                            <div style="float:right;">
                              <span class="dashicons dashicons-calendar-alt" style="font-size:16px"></span>
                              <?php echo date ("j F Y",$event_date);
                              ?>
                            </div>
                          <div class="events-tax">
                            <?php echo get_the_term_list( $post->ID, 'event_target', '<strong>Target Audience :</strong> ', ', ', '.  ' ) ?><br>
                            <?php echo get_the_term_list( $post->ID, 'event_industry', '<strong>Industry : </strong>', ', ', '.' ) ?>
                          </div>
                        </div>
                          <div class="events-content">
                            <a href="<?php the_permalink(); ?>">
                              <h2 class="events-title"><?php the_title(); ?></h2>
                            </a>
                            <?php
                              the_excerpt();
                            ?>
                          </div>
                      </div>
                <?php endwhile; ?>

                <!-- Display page navigation -->

            <?php global $wp_query;
            if ( isset( $wp_query->max_num_pages ) && $wp_query->max_num_pages > 1 ) { ?>
                <nav id="<?php echo $nav_id; ?>">
                    <div class="nav-previous"><?php next_posts_link( '<span class="meta-nav">&larr;</span> Older events'); ?></div>
                    <div class="nav-next"><?php previous_posts_link( 'Newer events <span class= "meta-nav">&rarr;</span>' ); ?></div>
                </nav>
            <?php };
        endif; ?>
      </div>
<br /><br />
<?php get_footer(); ?>
