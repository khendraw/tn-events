<?php
/**
* Plugin Name: TN Events
* Plugin URI: http://welinku21.net
* Description: This is plugin written for learning by doing - event custom post type
* Version: 1.00
* Author: K Hendrawan
* Author URI: http://welinku21.net
* License: GPL2
*/

/*
//For debugging only
register_activation_hook( __FILE__, 'my_activation_func' );

function my_activation_func() {
    file_put_contents( __DIR__ . '/my_loggg.txt', ob_get_contents() );
}*/

//Crete event post type
add_action( 'init', 'create_tn_event' );
function create_tn_event() {
    register_post_type( 'events',
        array(
            'labels' => array(
                'name' => 'Events',
                'singular_name' => 'Event',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Event',
                'edit' => 'Edit',
                'edit_item' => 'Edit Event',
                'new_item' => 'New Event',
                'view' => 'View',
                'view_item' => 'View Event',
                'search_items' => 'Search Event',
                'not_found' => 'No Event found',
                'not_found_in_trash' => 'No Event found in Trash',
                'parent' => 'Parent Event'
            ),
            'public' => true,
            'menu_position' => 15,
          'supports' => array( 'title', 'editor', 'thumbnail' ),
            'taxonomies' => array( '' ),
            //'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => true
        )
    );
    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}

//Add meta boxes

add_action( 'admin_init', 'my_admin' );

function my_admin() {
    add_meta_box( 'event_meta_box',
        'Event Details',
        'display_event_meta_box',
        'events', 'normal', 'high'
    );
}

function display_event_meta_box( $event ) {
    // Retrieve current date of event
    $event_date = esc_html( get_post_meta( $event->ID, 'event_date', true ) );
    ?>
    <table>
        <tr>
            <td>Event Date</td>
            <td><input type="date" name="event_date_input" value="<?php echo $event_date; ?>" /></td>
        </tr>
    </table>
    <?php
}

//Create CPT taxonomies
add_action( 'init', 'create_event_taxonomies', 0 );
function create_event_taxonomies() {
    register_taxonomy(
        'event_location',
        'events',
        array(
            'labels' => array(
                'name' => 'Event Location',
                'add_new_item' => 'Add New Event Location',
                'new_item_name' => "New Event Location"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
    register_taxonomy(
        'event_target',
        'events',
        array(
            'labels' => array(
                'name' => 'Event Target',
                'add_new_item' => 'Add New Event Target',
                'new_item_name' => "New Event Target"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
    register_taxonomy(
        'event_industry',
        'events',
        array(
            'labels' => array(
                'name' => 'Event Type',
                'add_new_item' => 'Add New Event Type',
                'new_item_name' => "New Event Type"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
}

// Save Post Field function

add_action( 'save_post', 'add_event_fields', 10, 2 );

function add_event_fields( $event_id, $event ) {
    // Check post type for events
    if ( $event->post_type == 'events' ) {
        // Store data in post meta table if present in post data
        if ( isset( $_POST['event_date_input'] ) && $_POST['event_date_input'] != '' ) {
            update_post_meta( $event_id, 'event_date', $_POST['event_date_input'] );
        }
    }
}

//Register Columns as Sortable on admin

add_filter( 'manage_edit-events_columns', 'events_columns' );

function events_columns( $columns ) {
    $columns['event_date'] = 'Event Date';
    unset( $columns['comments'] );
    return $columns;
}

add_action( 'manage_posts_custom_column', 'populate_columns' );

function populate_columns( $column ) {
    if ( 'event_date' == $column ) {
        $event_date = esc_html( get_post_meta( get_the_ID(), 'event_date', true ) );
        echo $event_date;
    }
}

add_filter( 'manage_edit-events_sortable_columns', 'event_date_sortable' );

function event_date_sortable ( $columns ) {
    $columns['event_date'] = 'event_date';
    return $columns;
}

add_filter( 'manage_edit-event_date_sortable_columns', 'event_date_orderby' );

function event_date_orderby( $query ) {
  if ( ! is_admin() )
    return;
    if ( isset( $vars['orderby'] ) && 'event_date' == $vars['orderby'] ) {
            $vars = array_merge( $vars, array( 'meta_key' => 'event_date', 'orderby' => 'meta_value' ) );
    }
    return $vars;
}

// Filter by custom taxonomy on admin

add_action( 'restrict_manage_posts', 'my_restrict_manage_posts' );

function my_restrict_manage_posts() {
    global $typenow, $post, $post_id;

	if( $typenow != "page" && $typenow != "post" ){
		//get post type
		$post_type=get_query_var('post_type');

		//get taxonomy associated with current post type
		$taxonomies = get_object_taxonomies($post_type);

		//in next loop add filter for tax
		if ($taxonomies) {
			foreach ($taxonomies as $tax_slug) {
				$tax_obj = get_taxonomy($tax_slug);
				$tax_name = $tax_obj->labels->name;
				$terms = get_terms($tax_slug);
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Show All $tax_name</option>";
				foreach ($terms as $term) {
					$label = (isset($_GET[$tax_slug])) ? $_GET[$tax_slug] : ''; // Fix
					echo '<option value='. $term->slug, $label == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
				}
				echo "</select>";
			}
		}
	}
}


// Add page template for event
add_filter( 'template_include', 'include_template_function', 1 );
function include_template_function( $template_path ) {
    if ( get_post_type() == 'events' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'single-events.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/templates/single-events.php';
            }
        }
        elseif ( is_archive() ) {
            if ( $theme_file = locate_template( array ( 'archive-events.php' ) ) ) {
                $template_path = $theme_file;
            } else { $template_path = plugin_dir_path( __FILE__ ) . '/templates/archive-events.php';

            }
        }
    }
    return $template_path;
}

//Change Excerpt length
function custom_excerpt_length( $length ) {
return 15;
}
add_filter( "excerpt_length", "custom_excerpt_length", 999 );

//Seperate styling to css file
function events_enqueue_style() {
	wp_enqueue_style( 'events', plugins_url( '/css/events-style.css', __FILE__ ) );
}
add_action( 'wp_enqueue_scripts', 'events_enqueue_style' );

// Adding Dashicons in WordPress Front-end
add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );
function load_dashicons_front_end() {
  wp_enqueue_style( 'dashicons' );
}
